const axios = require('axios');
async function getBooks() {
   const res = await axios.get('https://elibrary-api.herokuapp.com/v1/books')
      return res?.data?.books;
}
async function getBook(book_id) {
  const res = await axios.get(`https://elibrary-api.herokuapp.com/v1/book/${book_id}`)
     return res?.data?.book;
}
module.exports = {getBooks,getBook}