import logo from './logo.svg';
import Home from './views/Home';
import BookDetail from './views/BookDetail';
import './App.css';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {
  return (
    <div style={{ padding: 30 }}>
<h1>Books</h1>
<Router>
   <Routes>
     <Route path='/' element={<Home/>}/>
     <Route path='/book/:book_id' element={<BookDetail/>}/>
   </Routes>
</Router>
    </div>
   
  );
}

export default App;
