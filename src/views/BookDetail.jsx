import { getBook } from "../services";
import { useEffect,useState } from "react";
import { useParams } from "react-router-dom";

const BookDetail = () => {
    const {book_id} = useParams()
    const [book,setBook] = useState({})
    useEffect(() => {
        const exec = async () => {  
            console.log({book_id},"shweshwe")  
            const book_data = await getBook(book_id)
            setBook(book_data)
            console.log(book_data,"shweyi")  
        }
        exec()
         }, []);
         return (
             <div>
             <div>{book.title}</div>
             <div>{book.author}</div>
             <div>{book.description}</div>
             </div>
         )
}
export default BookDetail;