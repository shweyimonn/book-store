import { getBooks,getBook } from "../services";
import { useEffect,useState } from "react";
import {Link} from 'react-router-dom';
const Home = () => {
    const [books,setBooks] = useState([])
    
    useEffect(() => {
       const exec = async () => {    
           const res_data = await getBooks()
           setBooks(res_data)
           console.log(res_data,"sym") 
       }
       exec()
        }, []);
    return (
    <div>
         <table>
             <thead>
                <tr>
                <th>Title</th>
                 <th>Author</th>
                 <th>Description</th>
                 <th>&nbsp;</th>
                </tr>
                
             </thead>
            <tbody>
    {books.map((book) => {
        return (
           
            <tr key = {book.book_id}>
                <td>{book.title}</td>
                <td>{book.author}</td>
                <td>{book.description}</td>
                <td>
                    <Link to = {'/book/'+ book.book_id}>
                    <button>
                        Detail
                    </button>
                    </Link>
                </td>
            </tr>
            
        )
    } )}
    </tbody>
    </table>
    </div>
    )
   }
   export default Home;
   